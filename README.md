# Куда я попал?

Ты попал на страничку исходника, который поможет тебе встраивать в свои исходники AppsFlyer!
Этот исходник - пример кода, вы который должны реализовывать в своих приложениях.
Если вы скачали исход, почитали код и ничего не поняли, держите чистый код, который вы должны добавить в MainActivity:

https://pastebin.com/WCxc8fpL

https://pastebin.com/WCxc8fpL

https://pastebin.com/WCxc8fpL



## Как скачать исходник

Скачать исходный код очень просто, достаточно найти кнопку со значком скачивания слева от синий кнопки "Клонировать", выбрать в выпадающем меню пункт "zip" и нажать на него. 

![Как скачать исходник](https://res.cloudinary.com/wireforce/image/upload/v1647190309/Screenshot_2022-03-13_195139_wcoip2.png)

## Откуда мне брать код для работы с AppsFlyer 
Практически весь нужный вам код находится в файле **\StarterPack\app\src\main\java\com\example\starterpack\MainActivity.kt**

![](https://res.cloudinary.com/wireforce/image/upload/v1647190760/Screenshot_2022-03-13_195905_cpshwo.png)



## Как установить библиотеки прочую мишуру?

Документация по встраиванию AppsFlyer в приложения: https://dev.appsflyer.com/hc/docs/integrate-android-sdk



## У меня не такие же файлы Gradle, в оф. доке все по другому

В таком случае используйте то, что сделано в готовом примере из этого исходника, тут используется новый Gradle, который как-раз отличается от оф. документации апсфлайра    
